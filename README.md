# LatexRunequest

## Circular Calendar

This calendar represents a whole year in Glorantha. It includes holy days for many goddesses and gods of the Dragon Pass.

The source file includes three macros to display events, and time period. The calendar is quite easy to adapt to your needs.

It relies on XeTeX, and PGF/TikZ. You'll also need the Glorantha Core Runes font from [chaosium.com](https://wellofdaliath.chaosium.com/).

## Legal notice

These creations use trademarks and/or copyrights owned by Chaosium Inc/Moon Design Publications LLC, which are used under Chaosium Inc’s Fan Material Policy. I am expressly prohibited from charging you to use or access this content. These creations are not published, endorsed, or specifically approved by Chaosium Inc. For more information about Chaosium Inc’s products, please visit [www.chaosium.com](www.chaosium.com)
